<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `Compte` table method ------------------ */

    /**
     * Create a user 
     * @return int  user id success/fail
     */
    public function createUser($nom, $prenom, $fonction, $username, $password) {
        // Generating API key
        $api_key = $this->generateApiKey();

        // insert query
        $stmt = $this->conn->prepare("INSERT INTO Compte(nom, prenom, fonction, username, password, apiKey) values(?, ?, ?, ?, MD5(?), ?)");
        $stmt->bind_param("ssssss", $nom, $prenom, $fonction, $username, $password, $api_key);

        $last_id = -1;
        if($stmt->execute()){
            $last_id = $stmt->insert_id;
        }
        return $last_id;


    }

    /**
     * Checking user login
     * @return int user login status success/fail
     */
    public function checkLogin($username, $password) {
        // fetching user by username
        $stmt = $this->conn->prepare("SELECT idCompte FROM Compte WHERE username = ? AND password = MD5(?) ");
        $stmt->bind_param("ss", $username, $password);
        if($stmt->execute()) {
            $stmt->bind_result($idCompte);
            $stmt->fetch();
        } else {
            return -1;
        }
        return $idCompte;
    }

    
    /**
     * get all users
     * @return Array users
     */
    public function getUsers() {
        $stmt = $this->conn->prepare("SELECT idCompte, nom, prenom, fonction, username FROM Compte ");
        $stmt->execute();
        $users = $stmt->get_result();
        return $users;
    }

    /**
     * moddify a user
     * @return boolean success/fail
     */
    public function updateUser($idCompte, $nom, $prenom, $fonction, $username, $password){
        if(strlen($password) == 0){
            $query = "UPDATE Compte SET nom = ?, prenom = ?, fonction = ?, username = ? WHERE idCompte = ?";
            $stmt = $this->conn->prepare($query);
            $stmt->bind_param("ssssi", $nom, $prenom, $fonction, $username, $idCompte);

        }
        else{
            $query = "UPDATE Compte SET nom = ?, prenom = ?, fonction = ?, username = ?, password = MD5(?) WHERE idCompte = ?";
            $stmt = $this->conn->prepare($query);
            $stmt->bind_param("sssssi", $nom, $prenom, $fonction, $username, $password, $idCompte);
        }
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        return $num_affected_rows > 0;
    }

    /**
     * delete a user by id
     * @return boolean success/fail
     */
    public function deleteUser($idCompte){
        $query = "DELETE FROM Compte WHERE idCompte = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idCompte);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        return $num_affected_rows > 0;
    }
    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($idCompte) {
        $stmt = $this->conn->prepare("SELECT apiKey FROM Compte WHERE idCompte = ?");
        $stmt->bind_param("i", $idCompte);
        if ($stmt->execute()) {
            $stmt->bind_result($api_key);
            $stmt->fetch();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        if ($api_key == MOBILE_API_KEY)
            return 0;
        $stmt = $this->conn->prepare("SELECT idCompte FROM Compte WHERE apiKey = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        if($api_key == MOBILE_API_KEY)
            return true;
        $stmt = $this->conn->prepare("SELECT idCompte from Compte WHERE apiKey = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /* ------------- `Media` table method ------------------ */

    /**
     * Create a media
     * @return int media id success/fail
     */
    public function createMedia($idPdi, $lien, $nature, $description){
        $query = "INSERT INTO Media(idPdi, lien, nature, description) VALUES (?, ?, ?, ?)";

        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("isis", $idPdi, $lien, $nature, $description);

        $last_id = -1;
        if($stmt->execute()){
            $last_id = $stmt->insert_id;
        }

        $this->updateVersion();

        return $last_id;

    }
    /**
     * get all medias
     * @return mixed medias
     */
    public function getMedias(){
        $query = "SELECT idMedia, idPdi, lien, nature, description FROM Media";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $medias = $stmt->get_result();
        return $medias;
    }

    /**
     * delete a media by id
     * @return boolean success/fail
     */
    public function deleteMedia($idMedia){
        $query = "DELETE FROM Media WHERE idMedia = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idMedia);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;

        $this->updateVersion();

        return $num_affected_rows > 0;
    }

    /* ------------- `Type` table method ------------------ */

    /**
     * get all types
     * @return Array type
     */
    public function getTypes(){
        $query = "SELECT idType, titre, date FROM Type";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $types = $stmt->get_result();
        return $types;
    }
    
    /**
     * get type by id
     * @param mixed $idType 
     * @return mysqli_result
     */
    public function getType($idType){
        $query = "SELECT idType, titre, date FROM Type WHERE idType = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idType);
        $stmt->execute();
        $type = $stmt->get_result();
        return $type;
    }
    
    /**
     * Create a type
     * @return int type id success/fail
     */
    public function createType($titre){
        $query = "INSERT INTO Type(titre) VALUES(?)";

        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("s",$titre);

        $last_id = -1;
        if($stmt->execute()){
            $last_id = $stmt->insert_id;
        }

        $this->updateVersion();

        return $last_id;
    }

    /**
     * modify a type
     * @return boolean success/fail
     */
    public function updateType($idType, $titre){
        $query = "UPDATE Type SET titre = ? WHERE idType = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("si", $titre, $idType);

        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows; 

        $this->updateVersion();

        return $num_affected_rows > 0;
    }

    /**
     * delete a type by id
     * @return boolean success/fail
     */
    public function deleteType($idType){
        $query = "DELETE FROM Type WHERE idType = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idType);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;

        $this->updateVersion();
        
        return $num_affected_rows > 0;
    }

    /* ------------- `Pdi` table method ------------------ */
    /**
     * get all pois
     * @return Array pois
     */
    public function getPois() {
        $query = "SELECT * FROM Pdi";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $pois = $stmt->get_result();
        return $pois;
    }

    /**
     * get poi by id
     * @param mixed $idPdi 
     * @return mysqli_result
     */
    public function getPoi($idPdi) {
        $query = "SELECT * FROM Pdi WHERE idPdi = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idPdi);
        $stmt->execute();
        $poi = $stmt->get_result();
        return $poi;
    }
    
    /**
     * create a Poi
     * @return int poi id success/fail
     */
    public function createPoi($nom, $latitude, $longitude, $altitude, $description, $affichage, $idCompte, $idType){
        $query = "INSERT INTO Pdi(nom, latitude, longitude, altitude, description, affichage, idCompte, idType) VALUES
            (?, ?, ?, ?, ?, ?, ?, ?)";

        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("sdddsiii", $nom, $latitude, $longitude, $altitude, $description, $affichage, $idCompte, $idType);

        $last_id = -1;
        if($stmt->execute()){
            $last_id = $stmt->insert_id;
        }

        $this->updateVersion();
        
        return $last_id;
    }

    /**
     * modify a Poi
     * @return boolean success/fail
     */
    public function updatePoi($idPdi, $nom,  $latitude, $longitude, $altitude, $description, $affichage, $idCompte, $idType) {
        $query = "UPDATE Pdi SET nom = ?, latitude = ?, longitude = ?, altitude = ?, description = ?,
            affichage = ?, idCompte = ?, idType = ? WHERE idPdi = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("sdddsiiii", $nom, $latitude, $longitude, $altitude, $description, $affichage, $idCompte, $idType, $idPdi);

        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows; 

        $this->updateVersion();

        return $num_affected_rows > 0;
    }

    /**
     * delete a Poi by id
     * @return boolean success/fail
     */
    public function deletePoi($idPdi){
        $query = "DELETE FROM Media WHERE idPdi = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idPdi);
        $stmt->execute();
        
        $query = "DELETE FROM Pdi WHERE idPdi = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("i", $idPdi);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;

        $this->updateVersion();
        
        return $num_affected_rows > 0;
    }

    /*------------------------ Version ----------------------------*/

    public function getVersion(){
        $query = "SELECT * FROM  Version WHERE id = 1";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $version = $stmt->get_result();
        return $version;
    }
    
    
    private function updateVersion(){
        $query = "UPDATE Version SET version = version + 1 WHERE id = 1";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
    }
    
    
}

?>
