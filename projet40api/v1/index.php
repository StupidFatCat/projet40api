<?php

require_once '../include/DbHandler.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    // Getting request headers
    $response = array();
    $app = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    //$headers = parseRequestHeaders();
    //$headers['Authorization'] = "cc625d8fd2c17dc06a45df1b8cd607ec";
    $apiKey = $app->request->post('apiKey');
    if($apiKey == NULL)
        $apiKey = $app->request->get('apiKey');
    //$apikey = "cc625d8fd2c17dc06a45df1b8cd607ec";
    if (strlen($apiKey) == 32) {
        $db = new DbHandler();

        // get the api key
        $api_key = $apiKey;
        // validating api key
        
        if (!$db->isValidApiKey($api_key)){
            // api key is not present in users table
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            // get user primary key id
            global $user_id;
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["message"] = "Api key is missing";
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */


/**
 * User Login
 * url - /login
 * method - POST
 * params - username, password
 */
$app->post('/login', function() use ($app) {
            // check for required params
            verifyRequiredParams(array('username', 'password'));

            // reading post params
            $username = $app->request()->post('username');
            $password = $app->request()->post('password');
            $response = array();

            $db = new DbHandler();
            $idCompte = $db->checkLogin($username, $password);
            if ($idCompte != -1) {
                $apiKey = $db->getApiKeyById($idCompte);
                if ($apiKey != NULL) {
                    $response["idCompte"] = $idCompte;
                    $response["apiKey"] = $apiKey;
                    echoRespnse(200, $response);
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                    echoRespnse(400, $response);
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
                echoRespnse(400, $response);
            }

        });

/*
 * ------------------------ METHODS WITH AUTHENTICATION ------------------------
 */

/* --------------------- table `Compte` ----------------------------- */
/**
 * Create a user
 * url - /user
 * method - POST
 * params - nom, prenom, fonction, username, password
 */
$app->post('/user', 'authenticate', function() use ($app) {
            global $user_id;
            // if not mobile
            if($user_id != 0){
                // check for required params
                verifyRequiredParams(array('apiKey', 'nom', 'prenom', 'fonction', 'username', 'password'));

                $response = array();

                // reading post params
                $nom = $app->request->post('nom');
                $prenom = $app->request->post('prenom');
                $fonction = $app->request->post('fonction');
                $username = $app->request->post('username');
                $password = $app->request->post('password');


                $db = new DbHandler();
                $idCreate = $db->createUser($nom, $prenom, $fonction, $username, $password);

                if ($idCreate != -1) {
                    $response["idCreate"] = $idCreate;
                    // echo json response
                    echoRespnse(201, $response);
                } else {
                    $response["message"] = "Oops! An error occurred while creating.";
                    echoRespnse(400, $response);
                }
            }
        });
/**
 * List all users
 * url /users          
 * method GET
 */
$app->get('/users', 'authenticate', function() {
            global $user_id;
            // if not mobile
            if($user_id != 0){
                $response = array();
                $db = new DbHandler();

                // get all users
                $result = $db->getUsers();

                $response["users"] = array();

                // looping through result and preparing users array
                while ($user = $result->fetch_assoc()) {
                    $tmp = array();
                    $tmp["idCompte"] = $user["idCompte"];
                    $tmp["nom"] = $user["nom"];
                    $tmp["prenom"] = $user["prenom"];
                    $tmp["fonction"] = $user["fonction"];
                    $tmp["username"] = $user["username"];
                    array_push($response["users"], $tmp);
                }

                echoRespnse(200, $response);
            }
        });

/**
 * Modify a user
 * method PUT
 * params idCompte, nom, prenom, fonction, username, password
 * url - /users/:id
 */
$app->put('/user/:id', 'authenticate', function($idCompte) use($app) {
            global $user_id; 
            // if not mobile
            if($user_id != 0){
                // check for required params
                verifyRequiredParams(array('apiKey', 'nom', 'prenom', 'fonction', 'username', 'password'));


                $nom = $app->request->put('nom');
                $prenom = $app->request->put('prenom');
                $fonction = $app->request->put('fonction');
                $username = $app->request->put('username');
                $password = $app->request->put('password');

                $db = new DbHandler();
                $response = array();

                // updating user
                $result = $db->updateUser($idCompte, $nom, $prenom, $fonction, $username, $password);
                if ($result) {
                    // user updated successfully
                    $response["message"] = "User updated successfully";
                    echoRespnse(200, $response);
                } else {
                    // user failed to update
                    $response["message"] = "User failed to update. Please try again!";
                    echoRespnse(401, $response);
                }

            }
        });

/**
 * Delete a user
 * method DELETE
 * url /user
 */
$app->delete('/user/:id', 'authenticate', function($idCompte) use($app) {
            global $user_id;
            // if not mobile
            if ($user_id != 0){
                $db = new DbHandler();
                $response = array();
                $result = $db->deleteUser($idCompte);
                if ($result) {
                    // task deleted successfully
                    $response["message"] = "User deleted succesfully";
                    echoRespnse(200, $response);
                } else {
                    // task failed to delete
                    $response["message"] = "User failed to delete. Please try again!";
                    echoRespnse(400, $response);
                }
            }
        });


/* -------------------table `Media`-------------------------------*/
/**
 * List all medias
 * url /medias          
 * method GET
 */
$app->get('/medias', 'authenticate', function() {
            global $user_id;
            
            //mobile have access            
            $response = array();
            $db = new DbHandler();

            // get all users
            $result = $db->getMedias();

            $response["medias"] = array();

            // looping through result and preparing medias array
            while ($media = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["idMedia"] = $media["idMedia"];
                $tmp["idPdi"] = $media["idPdi"];
                $tmp["lien"] = $media["lien"];
                $tmp["nature"] = $media["nature"];
                $tmp["description"] = $media["description"];
                array_push($response["medias"], $tmp);
            }

            echoRespnse(200, $response);
            
        });

/**
 * Create a media
 * url - /media
 * method - POST
 * params - idPdi, lien, nature, description
 */
$app->post('/media', 'authenticate', function() use ($app) {
            global $user_id;
            // if not mobile
            if($user_id != 0){
                // check for required params
                verifyRequiredParams(array('apiKey', 'idPdi', 'lien', 'nature', 'description'));

                $response = array();

                // reading post params
                $idPdi = $app->request->post('idPdi');
                $lien = $app->request->post('lien');
                $nature = $app->request->post('nature');
                $description = $app->request->post('description');


                $db = new DbHandler();
                $idCreate = $db->createMedia($idPdi, $lien, $nature, $description);

                if ($idCreate != -1) {
                    $response["idCreate"] = $idCreate;
                    // echo json response
                    echoRespnse(201, $response);
                } else {
                    $response["message"] = "Oops! An error occurred while creating.";
                    echoRespnse(400, $response);
                }
            }
        });


/**
 * Delete a media 
 * method DELETE
 * url /media
 */
$app->delete('/media/:id', 'authenticate', function($idMedia) use($app) {
            global $user_id;
            // if not mobile
            if ($user_id != 0){
                $db = new DbHandler();
                $response = array();
                $result = $db->deleteMedia($idMedia);
                if ($result) {
                    // Media deleted successfully
                    $response["message"] = "Media deleted succesfully";
                    echoRespnse(200, $response);
                } else {
                    // Media failed to delete
                    $response["message"] = "Media failed to delete. Please try again!";
                    echoRespnse(400, $response);
                }
            }
        });

/* --------------------table `Type`----------------------*/

/**
 * List all types
 * url /types          
 * method GET
 */
$app->get('/types', 'authenticate', function() {
            global $user_id;
            
            //mobile have access            
            $response = array();
            $db = new DbHandler();

            // get all users
            $result = $db->getTypes();

            $response["types"] = array();

            // looping through result and preparing types array
            while ($type = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["idType"] = $type["idType"];
                $tmp["titre"] = $type["titre"];
                $tmp["date"] = $type["date"];
                array_push($response["types"], $tmp);
            }

            echoRespnse(200, $response);
            
        });
/**
 * get type by id
 * url /type
 * method GET
 */
$app->get('/type/:id', 'authenticate', function($idType) {
            global $user_id;
            
            //mobile have access            
            $db = new DbHandler();

            // get type by id 
            $result = $db->getType($idType);


            // looping through result and preparing type
            while ($type = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["idType"] = $type["idType"];
                $tmp["titre"] = $type["titre"];
                $tmp["date"] = $type["date"];
                $response = $tmp;
            }

            echoRespnse(200, $response);
            
        });

/**
 * Create a type
 * url - /type
 * method - POST
 * params - titre
 */
$app->post('/type', 'authenticate', function() use ($app) {
            global $user_id;
            // if not mobile
            if($user_id != 0){
                // check for required params
                verifyRequiredParams(array('apiKey', 'titre'));

                $response = array();

                // reading post params
                $titre = $app->request->post('titre');


                $db = new DbHandler();
                $idCreate = $db->createType($titre);

                if ($idCreate != -1) {
                    $response["idCreate"] = $idCreate;
                    // echo json response
                    echoRespnse(201, $response);
                } else {
                    $response["message"] = "Oops! An error occurred while creating.";
                    echoRespnse(400, $response);
                }
            }
        });


/**
 * Modify a type
 * method PUT
 * params idType, titre
 * url - /type/:id
 */
$app->put('/type/:id', 'authenticate', function($idType) use($app) {
            global $user_id; 
            // if not mobile
            if($user_id != 0){
                // check for required params
                verifyRequiredParams(array('apiKey', 'titre'));


                $titre = $app->request->put('titre');

                $db = new DbHandler();
                $response = array();

                // updating type
                $result = $db->updateType($idType, $titre);
                if ($result) {
                    // type updated successfully
                    $response["message"] = "Type updated successfully";
                    echoRespnse(200, $response);
                } else {
                    // type failed to update
                    $response["message"] = "Type failed to update. Please try again!";
                    echoRespnse(400, $response);
                }

                }
        });


/**
 * Delete a type 
 * method DELETE
 * url /type
 */
$app->delete('/type/:id', 'authenticate', function($idType) use($app) {
            global $user_id;
            // if not mobile
            if ($user_id != 0){
                $db = new DbHandler();
                $response = array();
                $result = $db->deleteType($idType);
                if ($result) {
                    // type deleted successfully
                    $response["message"] = "Type deleted succesfully";
                    echoRespnse(200, $response);
                } else {
                    // type failed to delete
                    $response["message"] = "Type failed to delete. Please try again!";
                    echoRespnse(400, $response);
                }
            }
        });


/* ---------------------table `Pdi`--------------------------*/

/**
 * List all pois
 * url /pois          
 * method GET
 */
$app->get('/pois', 'authenticate', function() {
            global $user_id;
            
            //mobile have access            
            $response = array();
            $db = new DbHandler();

            // get all users
            $result = $db->getPois();

            $response["pois"] = array();

            // looping through result and preparing pois array
            while ($poi = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["idPdi"] = $poi["idPdi"];
                $tmp["nom"] = $poi["nom"];
                $tmp["latitude"] = $poi["latitude"];
                $tmp["longitude"] = $poi["longitude"];
                $tmp["altitude"] = $poi["altitude"];
                $tmp["description"] = $poi["description"];
                $tmp["affichage"] = $poi["affichage"];
                $tmp["idCompte"] = $poi["idCompte"];
                $tmp["idType"] = $poi["idType"];
                $tmp["date"] = $poi["date"];
                array_push($response["pois"], $tmp);
            }

            echoRespnse(200, $response);
            
        });

/**
 * get poi by id
 * url /poi
 * method GET
 */
$app->get('/poi/:id', 'authenticate', function($idPdi) {
            global $user_id;
            
            //mobile have access            
            $db = new DbHandler();

            // get type by id 
            $result = $db->getPoi($idPdi);


            // looping through result and preparing type
            while ($poi = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["idPdi"] = $poi["idPdi"];
                $tmp["nom"] = $poi["nom"];
                $tmp["latitude"] = $poi["latitude"];
                $tmp["longitude"] = $poi["longitude"];
                $tmp["altitude"] = $poi["altitude"];
                $tmp["description"] = $poi["description"];
                $tmp["affichage"] = $poi["affichage"];
                $tmp["idCompte"] = $poi["idCompte"];
                $tmp["idType"] = $poi["idType"];
                $tmp["date"] = $poi["date"];
                $response = $tmp;
            }
            
            echoRespnse(200, $response);
            
        });

/**
 * Create a poi
 * url - /poi
 * method - POST
 * params - titre
 */
$app->post('/poi', 'authenticate', function() use ($app) {
            global $user_id;
            // if not mobile
            if($user_id != 0){
                // check for required params
                verifyRequiredParams(array('apiKey', 'nom', 'latitude', 'longitude', 'altitude', 'description', 'affichage', 'idCompte', 'idType'));

                $response = array();

                // reading post params
                $nom = $app->request->post('nom');
                $latitude = $app->request->post('latitude');
                $longitude = $app->request->post('longitude');
                $altitude = $app->request->post('altitude');
                $description = $app->request->post('description');
                $affichage = $app->request->post('affichage');
                $idCompte = $app->request->post('idCompte');
                $idType = $app->request->post('idType');


                $db = new DbHandler();
                $idCreate = $db->createPoi($nom, $latitude, $longitude, $altitude, $description, $affichage, $idCompte, $idType);

                if ($idCreate != -1) {
                    $response["idCreate"] = $idCreate;
                    // echo json response
                    echoRespnse(201, $response);
                } else {
                    $response["message"] = "Oops! An error occurred while creating.";
                    echoRespnse(400, $response);
                }
            }
        });


/**
 * Modify a poi
 * method PUT
 * params idPdi, nom, latitude, longitude, altitude, description, affichage, idCompte, idType 
 * url - /poi/:id
 */
$app->put('/poi/:id', 'authenticate', function($idPdi) use($app) {
            global $user_id; 
            // if not mobile
            if($user_id != 0){
                // check for required params
                verifyRequiredParams(array('apiKey', 'nom', 'latitude', 'longitude', 'altitude', 'description', 'affichage', 'idCompte', 'idType'));


                $nom = $app->request->put('nom');
                $latitude = $app->request->put('latitude');
                $longitude = $app->request->put('longitude');
                $altitude = $app->request->put('altitude');
                $description = $app->request->put('description');
                $affichage = $app->request->put('affichage');
                $idCompte = $app->request->put('idCompte');
                $idType = $app->request->put('idType');

                $db = new DbHandler();
                $response = array();

                // updating poi
                $result = $db->updatePoi($idPdi, $nom,  $latitude, $longitude, $altitude, $description, $affichage, $idCompte, $idType);
                if ($result) {
                    // poi updated successfully
                    $response["message"] = "Poi updated successfully";
                    echoRespnse(200, $response);
                } else {
                    // poi failed to update
                    $response["message"] = "Poi failed to update. Please try again!";
                    echoRespnse(400, $response);
                }

            }
        });


/**
 * Delete a poi 
 * method DELETE
 * url /poi
 */
$app->delete('/poi/:id', 'authenticate', function($idPdi) use($app) {
            global $user_id;
            // if not mobile
            if ($user_id != 0){
                $db = new DbHandler();
                $response = array();
                $result = $db->deletePoi($idPdi);
                if ($result) {
                    // poi deleted successfully
                    $response["message"] = "Poi deleted succesfully";
                    echoRespnse(200, $response);
                } else {
                    // poi failed to delete
                    $response["message"] = "Poi failed to delete. Please try again!";
                    echoRespnse(400, $response);
                }
            }
        });

/*--------------------- Version -----------------------*/

$app->get('/version', 'authenticate', function() {
            global $user_id;
            
            //mobile have access            
            $db = new DbHandler();

            // get Version 
            $result = $db->getVersion();


            // looping through result and preparing type
            while ($version = $result->fetch_assoc()) {
                $tmp = array();
                $tmp["version"] = $version["version"];
                $response = $tmp;
            }

            echoRespnse(200, $response);
            
        });

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            if($field != "password" && $field != "description" && $field != "altitude"){
                $error = true;
                $error_fields .= $field . ', ';
            }
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}


/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Array $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

function parseRequestHeaders() {
    $headers = array();
    foreach($_SERVER as $key => $value) {
        if (substr($key, 0, 5) <> 'HTTP_') {
            continue;
        }
        $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
        $headers[$header] = $value;
    }
    return $headers;
}

$app->run();
?>